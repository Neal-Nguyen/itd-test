function sum(stn1, stn2, temp) {
  var check = !isNaN(parseInt(stn1)) && !isNaN(parseInt(stn2));
  if (!check) return -1;
  return parseInt(stn1) + parseInt(stn2) + parseInt(temp);
}

function reverseString(str) {
  return str === "" ? "" : reverseString(str.substr(1)) + str.charAt(0);
}

function sumTwoBigNumber(stn1, stn2) {
  var result = "";
  if (typeof stn1 === "string" && typeof stn2 === "string") {
    var bigNum1 = "";
    var bigNum2 = "";
    if (stn1.length < stn2.length) {
      bigNum1 = reverseString(stn2);
      bigNum2 = reverseString(stn1);
    } else {
      bigNum1 = reverseString(stn1);
      bigNum2 = reverseString(stn2);
    }

    var lenBN1 = bigNum1.length;
    var temp = 0;

    for (var i = 0; i < lenBN1; i++) {
      if (bigNum2.length <= i) {
        if (sum(bigNum1[i], "0", temp) === -1) return "";
        var value = (sum(bigNum1[i], "0", temp) % 10).toString();
        temp = parseInt(sum(bigNum1[i], "0", temp) / 10);
        result = result.concat(value);
      } else {
        if (sum(bigNum1[i], bigNum2[i], temp) === -1) return "";
        var value = (sum(bigNum1[i], bigNum2[i], temp) % 10).toString();
        temp = parseInt(sum(bigNum1[i], bigNum2[i], temp) / 10);
        result = result.concat(value);
      }
    }
  }
  return reverseString(result);
}

console.log(sumTwoBigNumber("23", "456789")); // result sum
console.log(sumTwoBigNumber("t", "456789")); // result ""
console.log(sumTwoBigNumber(8, "456789")); // result ""
